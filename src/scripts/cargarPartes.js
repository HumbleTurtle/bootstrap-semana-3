function cargarParte( id, url ) {
    var cont = $(id);

    return new Promise( function( resolve, reject ) {
        jQuery.ajax({
            url
        }).done( function(data) {
            cont.html(data);
            resolve();
        });
    });
}

( async function () {
    await cargarParte("#myNav",   "/partes/cabecera.html");
    await cargarParte("#myFooter", "/partes/pie.html");

    localData.configure_nav();
}) ();